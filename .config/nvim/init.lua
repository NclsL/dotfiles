local load = function(mod)
  package.loaded[mod] = nil
  require(mod)
end

load('niclas.settings')
load('niclas.keymaps')
require('config.lazy')

-- set color scheme after installing everything
pcall(vim.cmd.colorscheme, 'catppuccin')
