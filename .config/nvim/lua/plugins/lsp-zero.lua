local Plugin = {'VonHeikemen/lsp-zero.nvim'}

Plugin.branch = 'v3.x'

function Plugin.config()
  local lsp_zero = require('lsp-zero')
  lsp_zero.on_attach(function(client, bufnr)
    -- see :help lsp-zero-keybindings
    -- to learn the available actions
    lsp_zero.default_keymaps({buffer = bufnr})
  end)
end

return Plugin
