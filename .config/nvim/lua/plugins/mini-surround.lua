return {
    'echasnovski/mini.surround',
    version = '*', -- Stable,
    opts = {
	-- The default 's' would conflict with leap
        mappings = {
            add = 'as', -- Add surrounding in Normal and Visual modes
            delete = 'ds', -- Delete surrounding
            replace = 'cs', -- Change/Replace surrounding
            -- I wont use these, but they have to be something else than s
            -- To not conflict with leap
            find = '999', -- Find surrounding (to the right)
            find_left = '999', -- Find surrounding (to the left)
            highlight = '999', -- Highlight surrounding
            update_n_lines = '999', -- Update `n_lines`

            suffix_next = '999', -- Suffix to search with "next" method
            suffix_last = '999', -- Suffix to search with "prev" method
        },
    }
}
