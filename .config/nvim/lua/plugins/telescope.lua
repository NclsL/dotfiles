return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.8',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'nvim-telescope/telescope-file-browser.nvim'
    },
    opts = {
        defaults = {
          prompt_prefix = " >",
          sorting_strategy = "ascending",
          color_devicons = true,
        },
        pickers = {
          find_files = {
            find_command = {'rg', '--files', '--no-ignore', '--hidden', '--glob', '!.git/*'}
          },
          live_grep = {
            vim_grep_arguments = {'rg', '--color=never', '--no-heading', '--with-filename', '--line-number', '--column', '--smart-case', '--hidden'}
          },
          grep_string = {
            vim_grep_arguments = {'rg', '--color=never', '--no-heading', '--with-filename', '--line-number', '--column', '--smart-case', '--hidden'}
           },
        }
    },
    extensions = {
        file_browser = {}
    }
}
