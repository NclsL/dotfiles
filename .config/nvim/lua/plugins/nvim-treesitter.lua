-- local Plugin = {"nvim-treesitter/nvim-treesitter"}
--
-- Plugin.opts = {
--   highlight = {
--     enable = true,
--   },
--   ensure_installed = "all",
--   ignore_install = { "robot", "phpdoc" },
--   sync_install = false,
--   indent = { enable = true },  
-- }
--
-- function Plugin.config(name, opts)
--   local configs = require("nvim-treesitter.configs")
--   configs.setup(opts)
-- end
--
-- return Plugin
--
return {
    "nvim-treesitter/nvim-treesitter",
    opts = {
        highlight = {
            enable = true,
        },
        ignore_install = { "robot", "phpdoc" },
        ensure_installed = "all",
        sync_install = false,
        indent = { enable = true },
    }
}
