return {
    "folke/which-key.nvim",
    -- event = "VeryLazy", -- This is too slow for me
    dependencies = { "echasnovski/mini.icons", opts={} },
    opts = {
        triggers = {
            { "<leader>", mode = { "n", "v" } },
        },
	spec = {
	    { "<leader>b", group = "buffers", nowait = false, remap = false },
	    { "<leader>bk", ":bd!<cr>", desc = "kill this", nowait = false, remap = false },
	    { "<leader>bl", "<cmd>Telescope buffers<cr>", desc = "list", nowait = false, remap = false },
	    { "<leader>bn", ":tabnew<cr>", desc = "new", nowait = false, remap = false },
	    { "<leader>bo", ":w|%bd|e#|bd#<cr>", desc = ":w kill others", nowait = false, remap = false },

	    { "<leader>d", group = "LaTeX & md", nowait = false, remap = false },
	    { "<leader>dC", ":VimtexClean<cr>", desc = "vimtex clean", nowait = false, remap = false },
	    { "<leader>dc", ":VimtexCompile<cr>", desc = "compile", nowait = false, remap = false },
	    { "<leader>dl", ":VimtexLabelsToggle<cr>", desc = "toggle labels", nowait = false, remap = false },

	    { "<leader>dt", group = "templates", nowait = false, remap = false },
	    { "<leader>dta", ":read ~/.config/nvim/templates/Article.tex<cr>", desc = "article", nowait = false, remap = false },
	    { "<leader>dtd", ":read ~/.config/nvim/templates/Default.tex<cr>", desc = "default", nowait = false, remap = false },
	    { "<leader>dth", ":read ~/.config/nvim/templates/HandOut.tex<cr>", desc = "homework", nowait = false, remap = false },
	    { "<leader>dv", ":VimtexView<cr>", desc = "view", nowait = false, remap = false },
	    { "<leader>dw", ":VimtexCountWords<cr>", desc = "count words", nowait = false, remap = false },

	    { "<leader>f", group = "file", nowait = false, remap = false },
	    { "<leader>fb", "<cmd>Telescope file_browser<cr>", desc = "file browser", nowait = false, remap = false },
	    { "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "find any file (hidden or .gitignored)", nowait = false, remap = false },
	    { "<leader>fp", "<cmd>Telescope git_files<cr>", desc = "git file in project", nowait = false, remap = false },
	    { "<leader>g", "<cmd>Neogit<cr>", desc = "git", nowait = false, remap = false },

	    { "<leader>l", group = "LSP", nowait = false, remap = false },
	    { "<leader>la", "<cmd>Telescope lsp_code_actions<cr>", desc = "actions", nowait = false, remap = false },
	    { "<leader>ld", "<cmd>Telescope lsp_definitions<cr>", desc = "definition", nowait = false, remap = false },
	    { "<leader>li", "<cmd>Telescope lsp_implementations<cr>", desc = "implementation", nowait = false, remap = false },
	    { "<leader>lp", "<cmd>Telescope diagnostics<cr>", desc = "problems", nowait = false, remap = false },
	    { "<leader>lt", "<cmd>Telescope lsp_type_definitions<cr>", desc = "type definition", nowait = false, remap = false },
	    { "<leader>o", "<cmd>Oil<cr>", desc = "Oil", nowait = false, remap = false },
	    { "<leader>p", ":lua require'telescope'.extensions.repo.list{file_ignore_patterns={\"/%.cache/\", \"/%.local/\", \"/%.cargo/\", \"/%autoload/\"}}<cr>", desc = "projectile", nowait = false, remap = false },

	    { "<leader>s", group = "search", nowait = false, remap = false },
	    { "<leader>sf", "<cmd>Telescope current_buffer_fuzzy_find<cr>", desc = "current file", nowait = false, remap = false },
	    { "<leader>sg", "<cmd>Telescope live_grep<cr>", desc = "current directory", nowait = false, remap = false },
	    { "<leader>su", "<cmd>Telescope grep_string<cr>", desc = "string under cursor", nowait = false, remap = false },

	    { "<leader>t", group = "toggles", nowait = false, remap = false },
	    { "<leader>tc", "<cmd>Telescope colorscheme<cr>", desc = "colorschemes", nowait = false, remap = false },
	    { "<leader>tr", ":set norelativenumber!<cr>", desc = "relative line nums", nowait = false, remap = false },
	    { "<leader>ts", ":let @/ = ''<cr>", desc = "search highlight", nowait = false, remap = false },

	    { "<leader>w", group = "windows", nowait = false, remap = false },
	    { "<leader>wc", ":close<cr>", desc = "close", nowait = false, remap = false },
	    { "<leader>wh", ":wincmd h<cr>", desc = "left", nowait = false, remap = false },
	    { "<leader>wj", ":wincmd j<cr>", desc = "down", nowait = false, remap = false },
	    { "<leader>wk", ":wincmd k<cr>", desc = "up", nowait = false, remap = false },
	    { "<leader>wl", ":wincmd l<cr>", desc = "right", nowait = false, remap = false },
	    { "<leader>wo", ":only<cr>", desc = "close others", nowait = false, remap = false },
	    { "<leader>ws", ":wincmd s<cr>", desc = "split", nowait = false, remap = false },
	    { "<leader>wv", ":wincmd v<cr>", desc = "vsplit", nowait = false, remap = false },
	    { "<leader>w1", ":1wincmd w<cr>", desc = "Jump to 1", nowait = false, remap = false },
	    { "<leader>w2", ":2wincmd w<cr>", desc = "Jump to 2", nowait = false, remap = false },
	    { "<leader>w3", ":3wincmd w<cr>", desc = "Jump to 3", nowait = false, remap = false },
	    { "<leader>w4", ":4wincmd w<cr>", desc = "Jump to 4", nowait = false, remap = false },
	    { "<leader>w5", ":5wincmd w<cr>", desc = "Jump to 5", nowait = false, remap = false },
	    { "<leader>w6", ":6wincmd w<cr>", desc = "Jump to 6", nowait = false, remap = false },
	}
    }
}
