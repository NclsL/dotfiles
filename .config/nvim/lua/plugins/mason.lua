local Plugin = {'williamboman/mason.nvim'}

Plugin.dependencies = {
  'williamboman/mason-lspconfig.nvim',
  'neovim/nvim-lspconfig'
}

Plugin.lazy = false

Plugin.opts = {
  ui = {border = 'rounded'}
}

function Plugin.config()
  local lsp_zero = require('lsp-zero')
  -- I dont really know yet what this even does, but it had to be extended
  -- to get rid of a warning
  lsp_zero.extend_lspconfig()

  require('mason').setup({})
  require('mason-lspconfig').setup({
    ensure_installed = {},
    handlers = {
      lsp_zero.default_setup,

      -- pylsp stop complaining abt line too long
      pylsp = function()
	require('lspconfig').pylsp.setup({
          settings = {
            pylsp = {
              plugins = {
                pycodestyle = {
                  ignore = {'E501'},
                }
              }
            }
          }
	})
      end
    },
  })
end

return Plugin
