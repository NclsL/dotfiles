local Plugin = {'hrsh7th/nvim-cmp'}

Plugin.dependencies = {
  -- work nicely with lsp
  {'VonHeikemen/lsp-zero.nvim'},

  -- Sources
  {'hrsh7th/cmp-buffer'},
  {'hrsh7th/cmp-path'},
  {'saadparwaiz1/cmp_luasnip'},
  {'hrsh7th/cmp-nvim-lsp'},
  {"lukas-reineke/cmp-under-comparator"},
  -- Snippets
  {'L3MON4D3/LuaSnip'},
  {'rafamadriz/friendly-snippets'},
  {"onsails/lspkind.nvim"}
}

function Plugin.config()
  local cmp = require('cmp')
  local lsp_zero = require('lsp-zero')
  local luasnip = require('luasnip')
  local lspkind = require('lspkind')

  require('luasnip.loaders.from_vscode').lazy_load()

  -- local select_opts = {behavior = cmp.SelectBehavior.Select}

  local cmp_action = lsp_zero.cmp_action()

  cmp.setup({
    mapping = cmp.mapping.preset.insert({
      -- `Enter` key to confirm completion
      ['<CR>'] = cmp.mapping.confirm({select = false}),
  
      -- Ctrl+Space to trigger completion menu
      ['<C-m>'] = cmp.mapping.complete(),
  
      -- Navigate between snippet placeholder
      ['<C-f>'] = cmp_action.luasnip_jump_forward(),
      ['<C-b>'] = cmp_action.luasnip_jump_backward(),
  
      -- Scroll up and down in the completion documentation
      ['<C-u>'] = cmp.mapping.scroll_docs(-4),
      ['<C-d>'] = cmp.mapping.scroll_docs(4),
    }),
    sources = {
      { name = "nvim_lsp" },
      { name = "path" },
      { name = "luasnip", keyword_length = 2 },
      { name = "buffer", keyword_length = 4 },
    },
    sorting = {
        comparators = {
          cmp.config.compare.offset,
          cmp.config.compare.exact,
          cmp.config.compare.score,
          require("cmp-under-comparator").under,
          cmp.config.compare.kind,
          cmp.config.compare.sort_text,
          cmp.config.compare.length,
          cmp.config.compare.order,
        },
    },
    formatting = {
      format = lspkind.cmp_format({
        with_text = true,
        menu = {
            buffer = "[buf]",
            nvim_lsp = "[lsp]",
            nvim_lua = "[vim]",
            path = "[path]",
            luasnip = "[snip]",
       },
      }),
    }
  })
end

return Plugin
