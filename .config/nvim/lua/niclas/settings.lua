local opt = vim.opt

opt.mouse = "a"
opt.showcmd = true
opt.cmdheight = 1

opt.termguicolors = true

opt.number = true
opt.relativenumber = true

opt.smartcase = true
opt.incsearch = true
opt.hlsearch = false
opt.ignorecase = true

opt.splitright = true
opt.splitbelow = true

opt.undofile = true
opt.scrolloff = 10

opt.list = true
opt.listchars = "tab:→ ,trail:·"

-- disable startup screen
opt.shortmess:append({ I = true })

opt.cursorline = true

-- tabs/spaces properly
opt.expandtab = true
opt.tabstop = 2
opt.shiftwidth = 2
opt.shiftround = 2
opt.smartindent = true
