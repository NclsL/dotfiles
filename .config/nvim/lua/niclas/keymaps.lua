-- Reselect visual selection after indenting
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")
vim.keymap.set("n", "<", "<S-v><gv")
vim.keymap.set("n", ">", "<S-v>>gv")

-- Maintain cursor position when yanking a visual selection
vim.keymap.set("v", "y", "myy`y")
vim.keymap.set("v", "Y", "myY`y")

-- Horizontal line movement
vim.keymap.set("n", "<S-h>", "g^")
vim.keymap.set("n", "<S-l>", "g$")
vim.keymap.set("v", "<S-h>", "g^")
vim.keymap.set("v", "<S-l>", "g$")

-- Drag lines
vim.keymap.set("n", "<A-j>", ":m .+1<CR>==")
vim.keymap.set("n", "<A-k>", ":m .-2<CR>==")
vim.keymap.set("i", "<A-j>", "<Esc>:m .+1<CR>==gi")
vim.keymap.set("i", "<A-k>", "<Esc>:m .-2<CR>==gi")
vim.keymap.set("v", "<A-j>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<A-k>", ":m '<-2<CR>gv=gv")

-- Return to save in normal
vim.keymap.set("n", "<CR>", ":w<CR>")

-- Stop jumping around during searches
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- fd to ESC
vim.keymap.set("i", "fd", "<Esc>")
vim.keymap.set("c", "qa", "qa!")
vim.keymap.set("n", "<S-y>", "y$")
