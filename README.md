### list of apps for quick setup

firefox  
alacritty  
zsh/fish  
zathura  
zathura-pdf-poppler  
LaTeX  
neovim  
ripgrep (grep)  
fd (find)  
ranger  
ncdu  
ffmpeg  
KDEconnect  
bat  
htop  
spotify  
libreoffice  
python  
pip (and neovim module with pip)  
zoxide
stow

Install in new repo for the first time

```
cd ~
git clone dotfiles......
cd dotfiles
stow --adopt .
```

Language servers  
gopls  
`GO111MODULE=on go get golang.org/x/tools/gopls@latest`
